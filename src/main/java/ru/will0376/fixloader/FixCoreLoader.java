package ru.will0376.fixloader;

import io.github.crucible.grimoire.common.api.GrimoireAPI;
import io.github.crucible.grimoire.common.api.grimmix.Grimmix;
import io.github.crucible.grimoire.common.api.grimmix.GrimmixController;
import io.github.crucible.grimoire.common.api.grimmix.lifecycle.IConfigBuildingEvent;
import io.github.crucible.grimoire.common.api.grimmix.lifecycle.ICoreLoadEvent;
import io.github.crucible.grimoire.common.api.grimmix.lifecycle.IModLoadEvent;
import io.github.crucible.grimoire.common.api.grimmix.lifecycle.IValidationEvent;
import io.github.crucible.grimoire.common.api.mixin.ConfigurationType;
import io.github.crucible.grimoire.common.api.mixin.IMixinConfigurationBuilder;
import lombok.SneakyThrows;
import ru.will0376.fixloader.utils.FixInfo;
import ru.will0376.fixloader.utils.FixLoad;
import ru.will0376.fixloader.utils.identifiers.FixIdentifierWrapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Grimmix(id = "fixloadergrimm")
public class FixCoreLoader extends GrimmixController {
	public static boolean skipLoading = Boolean.parseBoolean(System.getProperty("fixLoad.skipLoading", "false"));
	public static final String COMMIT_VERSION = "@Commit@";
	private FixLoad fixLoad;

	public FixCoreLoader() {
	}

	@Override
	public void validateController(IValidationEvent event) {
		fixLoad = new FixLoad(GrimoireAPI.getMinecraftFolder());
		if (!skipLoading) {
			fixLoad.findAllZips();
			fixLoad.extractZips();
		}
	}

	@SneakyThrows
	@Override
	public void buildMixinConfigs(IConfigBuildingEvent event) {
		event.createBuilder("fixload/mixins.fixloader.json")
				.mixinPackage("ru.will0376.fixloader.mixin")
				.commonMixins("*")
				.refmap("fixloader.refmap.json")
				.verbose(true)
				.required(true)
				.build();
		if (!skipLoading) {
			List<String> collect = FixLoad.getInfos()
					.stream()
					.map(e -> e.getIdListVanilla()
							.stream()
							.map(FixIdentifierWrapper::getFixClassName)
							.collect(Collectors.toList())).flatMap(Collection::stream).collect(Collectors.toList());

			event.createBuilder("fixload/mixins.fixload.json")
					.mixinPackage("fixes.mixins.vanilla")
					.commonMixins(collect.toArray(new String[]{}))
					.refmap("fixload.refmap.json")
					.verbose(true)
					.required(true)
					.build();

			List<String> registered = new ArrayList<>();
			for (FixInfo info : FixLoad.getInfos()) {
				if (registered.contains(info.getMod()))
					continue;
				registered.add(info.getMod());
				if (!info.checkDeps()) {
					System.err.println("Didn't find fix dependency: " + info.getDepsError());
					System.exit(1);
				}

				List<FixIdentifierWrapper> idListWithPredicate = FixLoad.getInfos().stream().filter(e -> e.getMod()
						.equals(info.getMod())).flatMap(e -> e.getIdListMixins().stream()).collect(Collectors.toList());
				IMixinConfigurationBuilder builder = event.createBuilder("fixLoad/mixins." + info.getMod() + ".json");

				if (!idListWithPredicate.isEmpty())
					builder.mixinPackage(idListWithPredicate.get(0).getFixPackage())
							.commonMixins(idListWithPredicate.stream()
									.map(FixIdentifierWrapper::getFixClassName)
									.collect(Collectors.toList())
									.toArray(new String[]{}))
							.configurationType(ConfigurationType.MOD)
							.verbose(true)
							.required(false)
							.build();
			}

		}
	}

	@Override
	public void coreLoad(ICoreLoadEvent event) {

	}

	@Override
	public void modLoad(IModLoadEvent event) {
		if (!skipLoading) {
			fixLoad.loadClasses();
		}
	}
}
