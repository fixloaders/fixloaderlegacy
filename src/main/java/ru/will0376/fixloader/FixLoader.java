package ru.will0376.fixloader;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ICrashCallable;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.discovery.ASMDataTable;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import io.github.crucible.omniconfig.api.OmniconfigAPI;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import org.apache.logging.log4j.Logger;
import ru.will0376.fixloader.command.FixLoaderCommand;
import ru.will0376.fixloader.mixin.IModAnnotation;
import ru.will0376.fixloader.utils.FixInfo;
import ru.will0376.fixloader.utils.FixLoad;

import java.text.SimpleDateFormat;
import java.util.Set;

@Mod(modid = FixLoader.MOD_ID, name = FixLoader.MOD_NAME, version = FixLoader.VERSION)
public class FixLoader {
	public static final String MOD_ID = "fixloader";
	public static final String MOD_NAME = "FixLoader";
	public static final String VERSION = "1.0";
	public static final String BUILD_HASH = "@Hash@";
	@Mod.Instance(MOD_ID)
	public static FixLoader INSTANCE;
	private Logger modLog;
	private Set<ASMDataTable.ASMData> fixAnnotationClasses;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) throws Exception {
		registerCrashHandler();
		modLog = event.getModLog();
		fixAnnotationClasses = event.getAsmData().getAll("ru.will0376.fixloader.utils.identifiers.Fix");
		if (!FixCoreLoader.skipLoading)
			for (ASMDataTable.ASMData asmData : fixAnnotationClasses) {
				if (asmData.getAnnotationInfo().containsKey("type")) {
					IModAnnotation type = (IModAnnotation) asmData.getAnnotationInfo().get("type");
					String fullClassName = asmData.getClassName();
					String substring = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
					switch (type.getValue()) {
						case "Event":
							event.getModLog().info("Loading event: " + substring);
							MinecraftForge.EVENT_BUS.register(Class.forName(asmData.getClassName()).newInstance());
							break;

						case "Config":
							event.getModLog().info("Loading config: " + substring);
							Class<?> annotationConfigClass = Class.forName(asmData.getClassName());
							if (!OmniconfigAPI.getAnnotationConfigRegistry()
									.getAssociatedOmniconfig(annotationConfigClass)
									.isPresent())
								OmniconfigAPI.registerAnnotationConfig(annotationConfigClass);
							break;
					}
				}
			}
	}

	private void registerCrashHandler() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
		FMLCommonHandler.instance().registerCrashCallable(new ICrashCallable() {
			@Override
			public String call() {
				StringBuilder builder = new StringBuilder();
				for (FixInfo info : FixLoad.getInfos()) {
					builder.append("\n")
							.append("[")
							.append(info.getMod())
							.append("] ")
							.append(info.getFixname())
							.append("-")
							.append(info.getBuildVersion())
							.append(" -> ")
							.append(simpleDateFormat.format(info.getCompileTime()));
				}

				return builder.append("\n").toString();
			}

			@Override
			public String getLabel() {
				return "\nLoaded fixes [Modid, Fixname-BuildVersion, BuildTime]";
			}
		});

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		modLog.info("Loading FixLoader Hash: " + BUILD_HASH);
	}

	@Mod.EventHandler
	public void event(FMLServerStartingEvent event) {
		event.registerServerCommand(new FixLoaderCommand());
		int errorCount = FixLoad.getErrorCount();
		if (errorCount >= 1)
			modLog.error(String.format("%s [FixLoader] There were %s errors loading the fixes", EnumChatFormatting.RED,
					errorCount));
	}

	public Set<ASMDataTable.ASMData> getFixAnnotationClasses() {
		return fixAnnotationClasses;
	}
}
