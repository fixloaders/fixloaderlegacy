package ru.will0376.fixloader.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import ru.will0376.fixloader.utils.FixInfo;
import ru.will0376.fixloader.utils.FixLoad;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FixLoaderCommand extends CommandBase {
	private static final Map<String, Boolean> crutches = new HashMap<>();

	public static boolean getCrutch(String name) {
		if (!crutches.containsKey(name)) {
			crutches.put(name, false);
			return false;
		}
		return crutches.get(name);
	}

	@Override
	public List<String> addTabCompletionOptions(ICommandSender p_71516_1_, String[] args) {
		if (args.length >= 1 && args[0].equals("crutch")) {
			return getListOfStringsMatchingLastWord(args, crutches.keySet().toArray(new String[]{}));
		}
		return getListOfStringsMatchingLastWord(args, "crutch", "crutchlist", "dumpFixLoaderList");
	}

	@Override
	public String getCommandName() {
		return "fixloader";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return "";
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (sender.canCommandSenderUseCommand(4, "fixloader")) {
			if (args.length >= 1) {
				switch (args[0]) {
					case "crutch":
						if (args.length == 2) {
							String arg = args[1];
							if (crutches.containsKey(arg)) {
								crutches.remove(arg);
								sender.addChatMessage(new ChatComponentText("Removed!"));
							} else {
								crutches.put(arg, true);
								sender.addChatMessage(new ChatComponentText("Added!"));
							}
						}
						break;

					case "crutchlist":
						sender.addChatMessage(new ChatComponentText("From List: "));
						crutches.forEach((s, b) -> sender.addChatMessage(new ChatComponentText(s + "--> " + b)));
						break;

					case "dumpFixLoaderList":
						for (FixInfo info : FixLoad.getInfos()) {
							System.out.println(info.toString());
						}
						break;
				}
			}
		}
	}
}
