package ru.will0376.fixloader.utils.identifiers;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Данный класс хранит всю информацию, которая предоставляется аннотацией @Fix
 */
@Data
public class FixIdentifierWrapper {
	private String fixPackage;
	private String fixClassName;
	private Fix.ClassType type;
	private Fix.UseSide side;
	private List<String> innerClass = new ArrayList<>();
	private int hash = 0;

	public FixIdentifierWrapper(Fix fix) {
		type = fix.type();
		side = fix.side();
	}

	public void setInnerClassesList(Map<String, Integer> map) {
		hash += map.entrySet().stream().flatMapToInt(e -> IntStream.of(e.getValue())).sum();
		if (!map.isEmpty())
			innerClass = new ArrayList<>(map.keySet());
	}

	public void addToHash(int hashIn) {
		this.hash += hashIn;
	}


	public String getFullClassName() {
		return fixPackage + "." + fixClassName;
	}
}
