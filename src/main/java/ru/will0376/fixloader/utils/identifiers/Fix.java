package ru.will0376.fixloader.utils.identifiers;

import io.github.crucible.grimoire.common.api.GrimoireAPI;
import io.github.crucible.grimoire.common.api.lib.Environment;
import ru.will0376.fixloader.utils.FixInfo;
import ru.will0376.fixloader.utils.FixLoad;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Fix {
	ClassType type() default ClassType.Mixin;

	String modid();

	String fixName();

	UseSide side() default UseSide.Server;

	String[] deps() default {};

	enum ClassType {
		Mixin,
		Event,
		Vanilla,
		Acceptor,
		Config,
		Other
	}

	enum UseSide {
		Client(Environment.CLIENT),
		Server(Environment.DEDICATED_SERVER),
		Universal(null);

		Environment side;

		UseSide(Environment side) {
			this.side = side;
		}

		public boolean checkSide() {
			if (this == Universal)
				return true;
			return side == GrimoireAPI.getEnvironment();
		}
	}

	class FixUtils {
		public static void initFix(Fix fix, Class<?> clazz) {
			FixInfo info = FixLoad.getOrCreateInfo(fix.modid(), fix.fixName());
			FixIdentifierWrapper fixIdentifierWrapper = info.initMixinClass(fix, clazz);
			if (fixIdentifierWrapper != null) {
				fixIdentifierWrapper.setFixClassName(clazz.getSimpleName());
				fixIdentifierWrapper.setFixPackage(clazz.getPackage().getName());
				fixIdentifierWrapper.setInnerClassesList(getInnerClasses(clazz));
				fixIdentifierWrapper.addToHash(clazz.hashCode());

				FixLoad.getLog().info("Init class => {}", clazz.getSimpleName());
			}
		}

		//Костыль для поиска вложенных классов, генерируемых компилятором.
		private static Map<String, Integer> getInnerClasses(Class<?> clazz) {
			Map<String, Integer> map = new HashMap<>();
			for (int i = 1; i <= 10; i++) {
				try {
					Class<?> aClass = Class.forName(clazz.getName() + "$" + i, false, clazz.getClassLoader());
					map.put("$" + i, aClass.hashCode());
				} catch (Exception ex) {
					return map;
				}
			}
			return map;
		}
	}

}
