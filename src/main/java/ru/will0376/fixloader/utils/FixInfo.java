package ru.will0376.fixloader.utils;

import lombok.Data;
import ru.will0376.fixloader.FixCoreLoader;
import ru.will0376.fixloader.utils.identifiers.Fix;
import ru.will0376.fixloader.utils.identifiers.FixIdentifierWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Data
public class FixInfo {
	private final transient List<Class<?>> tempListFixClasses = new ArrayList<>();
	String mod;
	String author = "Will0376";
	String description = "Just another fix";
	String fixname;
	int fixLoaderCommitCounter = -1;
	List<String> deps = new ArrayList<>();
	List<FixIdentifierWrapper> wrapperList = new ArrayList<>();
	String mcVersion = "1.7.10";
	long compileTime = 0;
	int hash = 0;
	int buildVersion = 0;
	private transient boolean skipFixVersionCheck = Boolean.parseBoolean(System.getProperty("fixloader.skipFixVersionCheck", "false"));
	private transient boolean skipDepsCheck = Boolean.parseBoolean(System.getProperty("fixloader.skipDepsCheck", "false"));

	public int countFixHashSum() {
		return hash = wrapperList.stream().mapToInt(FixIdentifierWrapper::getHash).sum();
	}

	public void setTime() {
		compileTime = System.currentTimeMillis();
	}

	public void incrementBuildVersion() {
		buildVersion++;
	}


	public FixInfo(String mod, String fixname) {
		this.mod = mod;
		this.fixname = fixname;
	}

	public static FixInfo infoFromString(String in) {
		return FixLoad.getGson().fromJson(in, FixInfo.class);
	}

	public boolean checkVersion() {
		if (skipFixVersionCheck)
			return true;
		if (FixCoreLoader.COMMIT_VERSION.equals("@Commit@") || fixLoaderCommitCounter == -1)
			return true;
		return Integer.parseInt(FixCoreLoader.COMMIT_VERSION) >= getFixLoaderCommitCounter();
	}

	public boolean hasVanillaFixes() {
		return wrapperList.stream().anyMatch(e -> e.getType() == Fix.ClassType.Vanilla);
	}

	public boolean hasEventFixes() {
		return wrapperList.stream().anyMatch(e -> e.getType() == Fix.ClassType.Event);
	}

	public List<FixIdentifierWrapper> getIdListWithPredicate(Predicate<? super FixIdentifierWrapper> p) {
		return getWrapperList().stream().filter(p).collect(Collectors.toList());
	}

	public boolean hasAcceptors() {
		return wrapperList.stream().anyMatch(e -> e.getType() == Fix.ClassType.Acceptor);
	}

	public List<FixIdentifierWrapper> getIdListMixins() {
		return getIdListWithPredicate(e -> e.getType() == Fix.ClassType.Mixin);
	}

	public List<FixIdentifierWrapper> getIdLisAcceptor() {
		return getIdListWithPredicate(e -> e.getType() == Fix.ClassType.Acceptor);
	}

	public List<FixIdentifierWrapper> getIdListEvent() {
		return getIdListWithPredicate(e -> e.getType() == Fix.ClassType.Event);
	}

	public List<FixIdentifierWrapper> getIdListVanilla() {
		return getIdListWithPredicate(e -> e.getType() == Fix.ClassType.Vanilla);
	}

	public List<FixIdentifierWrapper> getIdListOther() {
		return getIdListWithPredicate(e -> e.getType() == Fix.ClassType.Other);
	}

	public List<FixIdentifierWrapper> getIdLisConfig() {
		return getIdListWithPredicate(e -> e.getType() == Fix.ClassType.Config);
	}

	public boolean isClientFix() {
		return wrapperList.stream().anyMatch(e -> e.getSide() == Fix.UseSide.Client);
	}

	public boolean isServerFix() {
		return wrapperList.stream().anyMatch(e -> e.getSide() == Fix.UseSide.Server);
	}

	public boolean isUniversalFix() {
		return wrapperList.stream().anyMatch(e -> e.getSide() == Fix.UseSide.Universal);
	}

	public String stringFromInfo() {
		return FixLoad.getGson().toJson(this);
	}

	public boolean checkDeps() {
		if (skipDepsCheck)
			return true;
		if (deps.isEmpty())
			return true;
		return FixLoad.getFixNames().stream().anyMatch(e -> deps.contains(e));
	}

	public List<String> getDepsError() {
		return deps.stream().filter(e -> !FixLoad.getFixNames().contains(e)).collect(Collectors.toList());
	}

	public FixIdentifierWrapper initMixinClass(Fix fix, Class<?> clazz) {
		if (!tempListFixClasses.contains(clazz)) {
			tempListFixClasses.add(clazz);
			FixIdentifierWrapper e = new FixIdentifierWrapper(fix);
			wrapperList.add(e);
			for (String dep : fix.deps()) {
				if (!deps.contains(dep))
					deps.add(dep);
			}
			return e;
		}
		return null;
	}
}
