package ru.will0376.fixloader.utils;

import ru.will0376.fixloader.utils.identifiers.FixIdentifierWrapper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
	public static void unzipFix(ZipFile openedZip, File targetFolder) throws IOException {
		for (Enumeration<? extends ZipEntry> e = openedZip.entries(); e.hasMoreElements(); ) {
			ZipEntry zipEntry = e.nextElement();
			boolean isDirectory = zipEntry.isDirectory();
			Path newPath = zipSlipProtect(zipEntry, targetFolder.toPath());

			if (isDirectory) {
				Files.createDirectories(newPath);
			} else {
				if (newPath.getParent() != null) {
					if (Files.notExists(newPath.getParent())) {
						Files.createDirectories(newPath.getParent());
					}
				}
				extractFile(openedZip.getInputStream(zipEntry), newPath);
			}
		}
	}

	public static void packZipWithClasses(FixInfo info, File classFolder, File outputZip) {
		String json = FixLoad.getGson().toJson(info);
		try (ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(outputZip))) {
			ZipEntry entry = new ZipEntry("modfix.json");
			zout.putNextEntry(entry);
			zout.write(json.getBytes(StandardCharsets.UTF_8));
			for (FixIdentifierWrapper fixIdentifierWrapper : info.getWrapperList()) {
				if (!fixIdentifierWrapper.getInnerClass().isEmpty())
					for (String innerClass : fixIdentifierWrapper.getInnerClass()) {
						packClass(fixIdentifierWrapper.getFullClassName()
								.replace(".", "/") + innerClass + ".class", classFolder, zout);
					}
				packClass(fixIdentifierWrapper.getFullClassName().replace(".", "/") + ".class", classFolder, zout);
			}
			zout.closeEntry();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

	private static void packClass(String classname, File classFolder, ZipOutputStream zout) throws Exception {
		FileInputStream fis = new FileInputStream(new File(classFolder.getAbsolutePath(), classname));
		ZipEntry entry = new ZipEntry(classname);
		zout.putNextEntry(entry);
		byte[] buffer = new byte[fis.available()];
		fis.read(buffer);
		zout.write(buffer);
	}

	private static void extractFile(InputStream zipIn, Path filePath) throws IOException {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath.toFile()));
		byte[] bytesIn = new byte[4096];
		int read;
		while ((read = zipIn.read(bytesIn)) != -1) {
			bos.write(bytesIn, 0, read);
		}
		bos.close();
	}

	private static Path zipSlipProtect(ZipEntry zipEntry, Path targetDir) throws IOException {
		Path targetDirResolved = targetDir.resolve(zipEntry.getName());
		Path normalizePath = targetDirResolved.normalize();
		if (!normalizePath.startsWith(targetDir)) {
			throw new IOException("Bad zip entry: " + zipEntry.getName());
		}

		return normalizePath;
	}

	public static boolean isArchive(File f) {
		int fileSignature = 0;
		try (RandomAccessFile raf = new RandomAccessFile(f, "r")) {
			fileSignature = raf.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileSignature == 0x504B0304 || fileSignature == 0x504B0506 || fileSignature == 0x504B0708;
	}
}
