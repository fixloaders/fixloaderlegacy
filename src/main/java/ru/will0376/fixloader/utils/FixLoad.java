package ru.will0376.fixloader.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.github.crucible.grimoire.common.api.GrimoireAPI;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FixLoad {
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static final List<FixInfo> infos = new ArrayList<>();
	private static final Map<String, Boolean> modList = new HashMap<>();
	private static final Logger log = org.apache.logging.log4j.LogManager.getLogger("FixLoad");
	private static int errorCount;
	private static FixLoad instance;
	File rootdir;
	List<File> zipFileList = new ArrayList<>();

	public FixLoad(File file) {
		this.rootdir = new File(file.getAbsolutePath(), "Modfixer");
		instance = this;
	}

	public static Gson getGson() {
		return gson;
	}

	public static Logger getLog() {
		return log;
	}

	public static List<FixInfo> getInfos() {
		return infos;
	}

	public static FixLoad get() {
		return instance;
	}

	public static FixInfo getOrCreateInfo(String modname, String fixname) {
		FixInfo fixInfo = infos.stream()
				.filter(e -> e.getMod().equals(modname) && e.getFixname().equals(fixname))
				.findFirst()
				.orElse(null);
		if (fixInfo == null) {
			fixInfo = new FixInfo(modname, fixname);
			infos.add(fixInfo);
		}
		return fixInfo;
	}

	public static List<String> getFixNames() {
		return infos.stream().map(FixInfo::getFixname).collect(Collectors.toList());
	}

	public static void incrementError() {
		errorCount++;
		log.error("Loading error: #{}", errorCount);
	}

	public static int getErrorCount() {
		return errorCount;
	}

	public static Map<String, Boolean> getModList() {
		return modList;
	}

	public void findAllZips() {
		try {
			if (!rootdir.exists())
				rootdir.mkdir();
			List<Path> collect = Files.find(rootdir.toPath(), 999, (p, bfa) -> {
				String s = p.getFileName().toString();
				return bfa.isRegularFile() && (s.matches(".*\\.modfix") || s.matches(".*\\.zip"));
			}).collect(Collectors.toList());

			if (collect.isEmpty()) {
				log.error("Modfixer folder is empty");
				return;
			}
			zipFileList = collect.stream().map(Path::toFile).collect(Collectors.toList());
		} catch (IOException e) {
			incrementError();
			e.printStackTrace();
		}
	}

	public void extractZips() {
		try {
			for (File zipFile : zipFileList) {
				try {
					if (!ZipUtils.isArchive(zipFile)) {
						incrementError();
						log.error("{} file is not zip", zipFile.getName());
						continue;
					}
					try (ZipFile openedZipFile = new ZipFile(zipFile)) {
						ZipEntry entry = openedZipFile.getEntry("modfix.json");
						if (entry == null) {
							incrementError();
							log.error("{} archive has no identifier file", openedZipFile.getName());
							continue;
						}
						InputStream inputStream = openedZipFile.getInputStream(entry);

						FixInfo e = gson.fromJson(new InputStreamReader(inputStream), FixInfo.class);
						if (e.getFixLoaderCommitCounter() != -1 && !e.checkVersion()) {
							incrementError();
							log.error("Fix {} was written for the new version of modfixer! skipped load", e.getFixname());
							continue;
						}
						infos.add(e);
						if (!modList.containsKey(e.getMod()))
							modList.put(e.getMod(), e.hasVanillaFixes() || e.hasEventFixes());
						log.debug("Loaded {}-{} fix info", e.getFixname(), e.getBuildVersion());

						GrimoireAPI.getLaunchClassloader().addURL(zipFile.toPath().toUri().toURL());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadClasses() {
		try {
			ClassUtils.checkClasses();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
