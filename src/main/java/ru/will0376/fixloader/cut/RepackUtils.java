package ru.will0376.fixloader.cut;


import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import ru.will0376.fixloader.utils.ClassUtils;
import ru.will0376.fixloader.utils.FixInfo;
import ru.will0376.fixloader.utils.FixLoad;
import ru.will0376.fixloader.utils.ZipUtils;
import ru.will0376.fixloader.utils.identifiers.Fix;

import java.io.File;
import java.io.InputStream;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.zip.ZipFile;

public class RepackUtils {
	public static void repackFile(File inputfile, File outputFolder) {
		boolean skipModBuild = Boolean.parseBoolean(System.getProperty("skipModBuild", "false"));
		try {
			if (inputfile == null || outputFolder == null || inputfile.isDirectory())
				return;
			long first = System.currentTimeMillis();
			File parent = ClassUtils.tryGetParentFolder(inputfile);
			long start = System.currentTimeMillis();
			System.out.println("-:> Compiling the mod" + (Repackager.skipBuildIncrement ? " (without increment builversion)" :
					                                              ""));
			if (!skipModBuild) {
				ToolingAPI toolingAPI = new ToolingAPI();
				toolingAPI.executeTask("clean");
				toolingAPI.executeTask(ToolingAPI.GRADLE_TASK);
				System.out.println("-> Done! " + (System.currentTimeMillis() - start));
			} else
				System.out.println("[!] -> Skipped! ");

			File extractDir = new File(parent, "tmp");

			if (extractDir.exists())
				FileUtils.cleanDirectory(extractDir);
			else
				extractDir.mkdir();

			ZipFile zipFile = new ZipFile(inputfile);
			ZipUtils.unzipFix(zipFile, extractDir);
			URLClassLoader urlClassLoader = ClassUtils.loadClassesFromCompiledDirectory(ClassLoader.getSystemClassLoader(), extractDir);
			loadCompiledClasses(urlClassLoader);
			packCompiledClasses(outputFolder, extractDir);
			RepackConfig.saveStaticConfig();

			System.out.printf("[I] All done! (%d sec) have fun!\n", (System.currentTimeMillis() - first) / 1000);
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static int getCompiledGitCommitCount() {
		try {
			Process ps = Runtime.getRuntime().exec(new String[]{"git", "rev-list", "HEAD", "--count"}, null, new File("."));
			ps.waitFor();
			InputStream inputStream = ps.getInputStream();
			byte[] b = new byte[inputStream.available()];
			inputStream.read(b, 0, b.length);
			return Integer.parseInt(new String(b).trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	@SneakyThrows
	public static void loadCompiledClasses(ClassLoader classloader) {
		long start = System.currentTimeMillis();
		Reflections reflections = new Reflections("", classloader);
		Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(Fix.class);
		Set<String> typeAnnotationScanner = reflections.getStore().get(TypeAnnotationsScanner.class, Fix.class.getName());
		System.out.println("--> Founded classes: " + typesAnnotatedWith.size());
		if (typesAnnotatedWith.size() != typeAnnotationScanner.size()) {
			System.err.println("The size of the found classes does not match the size of the loaded ones. " + typeAnnotationScanner.size() + "/" + typesAnnotatedWith.size());
		}
		for (Class<?> aClass : typesAnnotatedWith) {
			Fix annotation = aClass.getAnnotation(Fix.class);
			if (aClass.isInterface() && annotation.type() != Fix.ClassType.Acceptor && annotation.type() != Fix.ClassType.Other)
				System.err.printf("Interface %s is annotated but is neither Acceptor nor Other", aClass.getName());
			System.out.printf("Fix: %s -> %s\n", annotation.fixName(), aClass.getName());
			Fix.FixUtils.initFix(annotation, aClass);
			typeAnnotationScanner.remove(aClass.getName());
		}

		if (!typeAnnotationScanner.isEmpty()) {
			for (String s : typeAnnotationScanner) {
				try {
					System.err.println(String.format("Class %s was not found initially, trying to fix", s));
					Class<?> aClass = classloader.loadClass(s);
					Fix annotation = aClass.getAnnotation(Fix.class);
					if (aClass.isInterface() && annotation.type() != Fix.ClassType.Acceptor && annotation.type() != Fix.ClassType.Other)
						System.err.printf("Interface %s is annotated but is neither Acceptor nor Other", aClass.getName());
					System.out.printf("Fix: %s -> %s\n", annotation.fixName(), aClass.getName());
					Fix.FixUtils.initFix(annotation, aClass);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("-> Done! " + (System.currentTimeMillis() - start));
	}

	@SneakyThrows
	public static void packCompiledClasses(File outputFolder, File classFolder) {
		long start = System.currentTimeMillis();
		System.out.println("-> Packing fixes");
		if (outputFolder.exists())
			FileUtils.cleanDirectory(outputFolder);
		else
			outputFolder.mkdir();
		int compiledGitCommitCount = getCompiledGitCommitCount();

		List<FixInfo> infos = FixLoad.getInfos();
		System.out.println("--> Found " + infos.size() + " fixes");
		List<File> filelist = new ArrayList<>();
		for (FixInfo info : infos) {
			File outputZipModFolder = new File(outputFolder, info.getMod());
			if (!outputZipModFolder.exists())
				outputZipModFolder.mkdir();

			if (!info.getMod().equals(info.getMod().toLowerCase(Locale.ROOT))) {
				System.err.println("Modid " + info.getMod() + " must be lowercase");
				System.exit(1);
			}

			info.setFixLoaderCommitCounter(compiledGitCommitCount);
			int hashSum = info.countFixHashSum();
			RepackConfig.FixVersion fixVersion = RepackConfig.get().findFixVersion(info.getFixname());
			info.setBuildVersion(fixVersion.getVer());
			if (!Repackager.skipBuildIncrement && fixVersion.getHash() != hashSum) {
				info.incrementBuildVersion();
				System.out.println(String.format("[II] incremented build version: %s, to %s", info.getFixname(), info.getBuildVersion()));
			}
			fixVersion.setHash(hashSum);
			fixVersion.setVer(info.getBuildVersion());
			RepackConfig.get().replaceVersionInList(fixVersion);
			info.setTime();
			String add = "";

			if (info.isClientFix())
				add = "[Client]";
			if (info.isServerFix())
				add = "[Server]";
			if (info.isUniversalFix())
				add += "[Universal]";


			if (info.hasVanillaFixes() || info.getMod().contains("vanilla"))
				add += "[Vanilla]";

			if (!info.getMod().contains("vanilla"))
				add += "[" + info.getMod() + "]";

			if (info.hasEventFixes())
				add += "[Events]";
			File outputZip = new File(outputZipModFolder, add + info.getFixname() + ".modfix");
			filelist.add(outputZip);
			ZipUtils.packZipWithClasses(info, classFolder, outputZip);
		}

		File outputAllZipFolder = new File(outputFolder, "ALL");
		if (outputAllZipFolder.exists())
			FileUtils.cleanDirectory(outputAllZipFolder);
		else
			outputAllZipFolder.mkdir();

		for (File file : filelist) {
			Files.copy(file.toPath(), new File(outputAllZipFolder, file.getName()).toPath());
		}
		System.out.println("-> Done! " + (System.currentTimeMillis() - start));
	}
}
