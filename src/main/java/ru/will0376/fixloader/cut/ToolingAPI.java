package ru.will0376.fixloader.cut;

import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;

import java.nio.file.Paths;

public class ToolingAPI {
	public static final String GRADLE_TASK = "build";

	private final GradleConnector connector;

	public ToolingAPI() {
		connector = GradleConnector.newConnector();
		RepackConfig config = RepackConfig.get();
		connector.useInstallation(Paths.get(config.getGradleFolder()).toFile());
		connector.forProjectDirectory(Paths.get(config.getFixProjectFolder()).toFile());
	}

	public void executeTask(String... tasks) {
		ProjectConnection connection = connector.connect();
		BuildLauncher build = connection.newBuild();
		build.forTasks(tasks);
		build.run();
		connection.close();
	}
}
