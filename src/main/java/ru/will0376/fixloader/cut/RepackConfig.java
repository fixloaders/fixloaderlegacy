package ru.will0376.fixloader.cut;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

@Data
public class RepackConfig {
	private static transient RepackConfig instance;
	private static transient File configFile = new File(".", System.getProperty("fixrepacker.configfile.name", "repack-1.7.10.json"));
	private String selectedFolderInput;
	private String selectedFolderOutput;
	private String fixProjectFolder;
	private String gradleFolder;
	private List<FixVersion> versions = new ArrayList<>();

	private RepackConfig() {
	}

	public static RepackConfig get() {
		return instance;
	}

	public static RepackConfig loadConfig() {
		if (!configFile.exists()) {
			return instance = new RepackConfig();
		}
		try {
			return instance = new Gson().fromJson(new FileReader(configFile), RepackConfig.class);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static File getFileFromString(String in) {
		if (in == null)
			return null;
		return new File(in);
	}

	public static void saveStaticConfig() {
		get().saveConfig();
	}

	public void saveConfig() {
		String s = new GsonBuilder().setPrettyPrinting().create().toJson(this);
		try (FileWriter fileWriter = new FileWriter(configFile)) {
			fileWriter.write(s);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public FixVersion findFixVersion(String name) {
		return versions.stream().filter(e -> e.getFixName().equals(name)).findFirst().orElse(new FixVersion(name, 0, 0));
	}

	public void replaceVersionInList(FixVersion version) {
		versions.removeIf(e -> e.getFixName().equals(version.getFixName()));
		versions.add(version);
	}

	@AllArgsConstructor
	@Data
	public static class FixVersion {
		private String fixName;
		private int hash;
		private int ver;
	}
}
