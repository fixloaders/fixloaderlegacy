package ru.will0376.fixloader.mixin;

import cpw.mods.fml.common.discovery.asm.ModAnnotation;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(ModAnnotation.EnumHolder.class)
public interface IModAnnotation {
	@Accessor
	String getDesc();

	@Accessor
	String getValue();
}
